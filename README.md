Just a fork (copy) of: https://github.com/MSOpenTech/odataphpprod that can be installed with composer

How to use
----------

Instal composer if you don't have it: `curl -sS https://getcomposer.org/installer | php`.
Also, phpbrew is good to have to manage PHP version. This package supports PHP version 5.3.x 
only. Follow the instructions [here](https://github.com/phpbrew/phpbrew).

composer.json

```
{
  "require": {
    "gizur/odataphpprod": "dev-master"
  }
}
```


Notes
----

I had to install some dependencies for `phpbrew` to work: 
`sudo apt-get install -y libxml2-dev libbz2-dev libmcrypt-dev libxslt1-dev pear`
Then install version 5.3.28: `phpbrew install 5.3.28 +default+openssl` and 
`phpbrew use php-5.3.28`.

